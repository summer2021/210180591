# 环境安装

安装gcc工具链: `sudo apt install gcc-arm-none-eabi` 

qemu: `sudo apt install qemu-system`

rust: nightly版，需要添加"armv7a-none-eabi"工具链，`rustup target add armv7a-none-eabi`

# 如何运行

`make` 将会进行build

`make qemu-dbg` 将会启动qemu和gdb进行调试。

# 文件组织

rt_kernel文件夹包含了移植的内核源文件。

boot文件夹中包含了原来的官方驱动和启动文件，包括bsp和libcpu文件夹的下的相关文件。

include包含官方的一些头文件，主要用于编译C时进行引用。

components包含串口的官方驱动。

tools包含了Make的一些函数定义.
