use crate::my_app;
use crate::{irq::rt_hw_interrupt_disable, rt_extern::*};
use crate::mem::init_heap;
use crate::idle::rt_thread_ilde_init;

#[no_mangle]
pub extern "C" fn rtthread_startup() -> !{
    unsafe {
        rt_hw_interrupt_disable();
        dbg_uart0_init();
        rt_hw_board_init();
        init_heap();
        rt_components_board_init();
    }
    rt_thread_ilde_init();
    my_app();
    loop {}
}

#[no_mangle]
pub extern "C" fn rt_components_board_init() {
    extern "C" {
        pub fn rt_hw_timer_init() -> i32;
    }
    unsafe {
        rt_hw_timer_init();
    }
    
}
