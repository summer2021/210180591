use alloc::string::String;

#[derive(Debug)]
pub struct RtObject {
    name: String,
    t: RtObjectClassType,
    flag: u8
}

#[derive(Debug)]
pub enum RtObjectClassType
{
    RtObjectClassNull          , 
    RtObjectClassThread        , 
    RtObjectClassSemaphore     , 
    RtObjectClassMutex         , 
    RtObjectClassEvent         , 
    RtObjectClassMailBox       , 
    RtObjectClassMessageQueue  , 
    RtObjectClassMemHeap       , 
    RtObjectClassMemPool       , 
    RtObjectClassDevice        , 
    RtObjectClassTimer         , 
    RtObjectClassModule        , 
    RtObjectClassUnknown       ,      
    RtObjectClassStatic        ,       
}

impl RtObject {
    pub fn new(name: String, t: RtObjectClassType) -> Self {
        RtObject{
            name,
            t,
            flag: 0,
        }
    }

    pub fn set_flag(&mut self, flag: u8) {
        self.flag = flag;
    }

    pub fn flag(&self) -> u8 {
        self.flag
    }

}