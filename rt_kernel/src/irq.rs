#[no_mangle]
static mut RT_INTERRUPT_NEST: u8 = 0;

extern "C" {
    pub fn rt_hw_interrupt_disable() -> isize;
    pub fn rt_hw_interrupt_enable(level: isize);
}

pub fn get_interrupt_nest() -> u8 {
    unsafe {
        RT_INTERRUPT_NEST
    }
}


#[no_mangle]
pub extern "C" fn rt_interrupt_enter() {
    unsafe {
        let level = rt_hw_interrupt_disable();
        RT_INTERRUPT_NEST += 1;
        rt_hw_interrupt_enable(level);
    }

    return;
}

#[no_mangle]
pub extern "C" fn rt_interrupt_leave() {
    unsafe {
        let level = rt_hw_interrupt_disable();
        RT_INTERRUPT_NEST -= 1;
        rt_hw_interrupt_enable(level);
    }

    return;
}