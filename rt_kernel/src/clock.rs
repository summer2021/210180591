use crate::{irq::rt_hw_interrupt_disable, println, scheduler::*, thread::*, timer::*};

static mut RT_TICK: usize = 0;


#[no_mangle]
pub extern "C" fn rt_tick_increase() {
    unsafe {
        RT_TICK += 1;
        println!("tick = {}", RT_TICK); // 调试
    }

    if let Some(cur_thread) = get_cur_thread(){
        cur_thread.borrow_mut().tick_minus(1);
    
        if cur_thread.borrow().tick_run_out() {
            cur_thread.borrow_mut().remaining_tick_reset();
            
            rt_schedule();
        }
    }
    
    rt_timer_check();
}

pub fn rt_tick_get() -> usize {
    unsafe {
        RT_TICK
    }
}