use core::{ffi::c_void, fmt::{Display, Error, write}};

use alloc::fmt;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum RtErrCode {
    RtError    ,              //< A generic error happens 
    RtEtimeout ,              //< Timed out 
    RtEfull    ,              //< The resource is full 
    RtEempty   ,              //< The resource is empty 
    RtEnomem   ,              //< No memory 
    RtEnosys   ,              //< No system 
    RtEbusy    ,              //< Busy 
    RtEio      ,              //< IO error 
    RtEintr    ,              //< Interrupted system call 
    RtEinval                 //< Invalid argument
}

impl Display for RtErrCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error!")
    }
}

/// 包含libcpu, bsp驱动等一些调用函数
extern "C" {
    pub fn rt_hw_interrupt_init();
    pub fn  rt_hw_board_init();
    // rt_uint8_t *rt_hw_stack_init(void *tentry, void *parameter, rt_uint8_t *stack_addr, void *texit)
    pub fn rt_hw_stack_init(tentry: *const c_void, parameter: *const c_void, stack_addr: *mut u8, texit: *const c_void) -> *const c_void;
    pub fn dbg_uart0_init();
    pub fn dbg_uart_putc(c: u8);
    pub fn idle_wfi();
}

// 解决rust报错
#[no_mangle]
pub extern fn __aeabi_unwind_cpp_pr0() {}