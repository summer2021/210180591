use core::{cell::RefCell, ffi::c_void, ptr::null};

use alloc::{string::ToString, sync::Arc};

use crate::{print, rt_extern::idle_wfi, thread::*};

const IDLE_THREAD_STACK_SIZE: usize = 512;

static mut IDLE_STACK:[u8; IDLE_THREAD_STACK_SIZE] = [0; IDLE_THREAD_STACK_SIZE];

#[no_mangle]
fn idle_wfi_thread() {
    loop {
        unsafe {
            idle_wfi();
        }
    }
}

pub fn rt_thread_ilde_init() {
    let entry = idle_wfi_thread as *const c_void;
    let t = Arc::new(
        RefCell::new(
            Thread::init(
                "IDLE".to_string(),
                entry, 
                null(), 
                unsafe {&mut IDLE_STACK[0] as *mut u8}, 
                IDLE_THREAD_STACK_SIZE, 
                RT_THREAD_PRIORITY_MAX-1, 
                10
            )
        )
    );

    push_thread(t.clone());
    push_prio_table(t);
}