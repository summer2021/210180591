use core::cell::RefCell;

use alloc::{collections::{BTreeSet, LinkedList}, sync::Arc};

use crate::thread::Thread;

pub mod sem;
pub mod msg_queue;

#[derive(Debug)]
pub enum IpcFlag {
    RtIpcPrio(BTreeSet<Arc<RefCell<Thread>>>),
    RtIpcFifo(LinkedList<Arc<RefCell<Thread>>>),
}

trait Ipc {
    fn ipc_suspend(&mut self, thread: Arc<RefCell<Thread>>) {

    }

    fn ipc_list_resume(&mut self);

    fn ipc_list_resume_all(&mut self);
}